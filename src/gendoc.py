#!/usr/bin/env python3
#
# This program and the accompanying materials
# are made available under the terms of the Apache License, Version 2.0
# which accompanies this distribution, and is available at
#
# http://www.apache.org/licenses/LICENSE-2.0
#
"""
    programme de generation de fichier docx
    à partir de fichier/données excel
    pour les besoins de l'association ROSPERED
"""
from docx import Document
from docx.enum.text import WD_ALIGN_PARAGRAPH
from docx.shared import Pt
from docx.shared import RGBColor
from xlrd import open_workbook

# ouverture du fichier contenant les données sources
donnees_source = open_workbook('../fichier-source/data.xls')
sheet = donnees_source.sheet_by_index(0)
sheet.cell_value(0, 0)
noms_parcelles = []

for elem in sheet.col(7):
    noms_parcelles.append(elem.value.lower())

noms_parcelles_reduite = sorted(list(set(noms_parcelles)))
print(len(noms_parcelles_reduite))

with open("liste_nom_parcelles.csv", "w") as fichier:
    for elem in noms_parcelles_reduite:
	    fichier.write(elem+'\n')

infos_par_nom_parcelles = []

for nom_parcelle in noms_parcelles_reduite:
    infos_par_nom_parcelle = {}
    infos_par_nom_parcelle['nom_parcelle'] = nom_parcelle
    infos_par_nom_parcelle['parcelles'] = []
    for idx, ligne in enumerate(sheet.col(7)):
        if ligne.value.lower() == nom_parcelle:
            infos_parcelle = {}
            infos_parcelle['id_parcelle'] = sheet.cell_value(idx,5)
            infos_parcelle['nom_parcelle'] = sheet.cell_value(idx,7)
            infos_parcelle['lieudit'] = sheet.cell_value(idx,2)
            infos_parcelle['proprietaire'] = sheet.cell_value(idx,0)
            infos_parcelle['nature'] = sheet.cell_value(idx,8)
            infos_parcelle['photo'] = sheet.cell_value(idx,9)
            infos_par_nom_parcelle['parcelles'].append(infos_parcelle)
    infos_par_nom_parcelles.append(infos_par_nom_parcelle)

print(infos_par_nom_parcelles[3])

# ouverture du fichier que l'on souhaite créer
fichier_cree = open("../resultat/microtoponymie_rospez.docx", "w")
document = Document()

# le titre du document
titre_doc = document.add_paragraph()
titre_doc.alignment = WD_ALIGN_PARAGRAPH.CENTER
texte_titre_doc = titre_doc.add_run('Fichier de travail de microtoponymie commune de Rospez-ed2\n')
texte_titre_doc.bold = True
texte_titre_doc.font.name = 'Calibri'
texte_titre_doc.font.size = Pt(14)

for idx, parcelle_unique in enumerate(infos_par_nom_parcelles):
    # if idx == 20:
    #     break
    # le nom de parcelle étudiée
    titre_nom_parcelle = document.add_paragraph()
    titre_nom_parcelle.alignment = WD_ALIGN_PARAGRAPH.CENTER
    texte_titre_nom_parcelle = titre_nom_parcelle.add_run(parcelle_unique['nom_parcelle'])
    texte_titre_nom_parcelle.bold = True
    texte_titre_nom_parcelle.font.name = 'Calibri'
    texte_titre_nom_parcelle.font.size = Pt(12)
    texte_titre_nom_parcelle.font.color.rgb = RGBColor(0xDC, 0x14, 0x3C)
    texte_titre_nom_parcelle.font.underline = True

    # la partie concernant les numeros de parcelle et localisation des parcelles
    # ayant le nom étudié
    section_titre1 = document.add_paragraph()
    texte_titre_section1 = section_titre1.add_run('Numéros des parcelles-localisation en 1828')
    texte_titre_section1.bold = True

    contenu_section1 = document.add_paragraph()
    # contenu_section1.paragraph_format.space_after = Pt(10)
    for idx, parcelle in enumerate(parcelle_unique['parcelles']):
        texte_contenu_section1 = contenu_section1.add_run(parcelle['id_parcelle'])
        texte_contenu_section1.font.name = 'Calibri'
        texte_contenu_section1.font.size = Pt(8)
        if idx != len(parcelle_unique['parcelles'])-1:
            texte_contenu_section1 = contenu_section1.add_run(', ')


    # la partie concernant l'etymologie/traduction de ce nom de parcelle
    section_titre2 = document.add_paragraph()
    texte_titre_section2 = section_titre2.add_run('Etymologie/traduction (a remplir par le groupe)')
    texte_titre_section2.bold = True

    contenu_section2 = document.add_paragraph()
    texte_contenu_section2 = contenu_section2.add_run('\n')
    texte_contenu_section2 = contenu_section2.add_run('\n')


    # la partie concernant les infos de chaque parcelle ayant ce nom
    section_titre3 = document.add_paragraph()
    texte_titre_section3 = section_titre3.add_run('Numéro, description, propriétaire de la parcelle, lieu-dit, noms photo')
    texte_titre_section3.bold = True

    contenu_section3 = document.add_paragraph()
    for idx, parcelle in enumerate(parcelle_unique['parcelles']):
        texte_contenu_section3 = contenu_section3.add_run(parcelle['id_parcelle'])
        texte_contenu_section3.font.name = 'Calibri'
        texte_contenu_section3.font.size = Pt(8)
        texte_contenu_section3 = contenu_section3.add_run(': ' + parcelle['nature'])
        texte_contenu_section3.font.name = 'Calibri'
        texte_contenu_section3.font.size = Pt(8)
        texte_contenu_section3 = contenu_section3.add_run(': ' + parcelle['proprietaire'])
        texte_contenu_section3.font.name = 'Calibri'
        texte_contenu_section3.font.size = Pt(8)
        texte_contenu_section3 = contenu_section3.add_run(': ' + parcelle['lieudit'])
        texte_contenu_section3.font.name = 'Calibri'
        texte_contenu_section3.font.size = Pt(8)
        texte_contenu_section3 = contenu_section3.add_run(': ' + parcelle['photo'])
        texte_contenu_section3.font.name = 'Calibri'
        texte_contenu_section3.font.size = Pt(8)
        texte_contenu_section3 = contenu_section3.add_run('\n')



# on sauve et ferme le document
document.save('../resultat/microtoponymie_rospez.docx')
fichier_cree.close()
